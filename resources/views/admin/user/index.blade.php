@extends('admin.layouts.main')
@section('title','Lista de Usuarios')
@section('content')
<a href="{{route('user.create')}}" class="btn btn-primary">Nuevo Usuario</a>



{!! Form::open(['route'=>'user.index', 'method'=>'GET',
                'CLASS'=>'navbar-form pull-right'] ) !!}
<div class="form-group">      
    <div class="input-group">
        {!! Form::text('nombre',null, ['class'=>'form-control', 
                                      'placeholder'=>'Buscar por nombre de usuario']) !!}        
        <div class="input-group-addon">
            <span class="glyphicon glyphicon-search"></span>
        </div>
    </div>
</div>     
{!! Form::close() !!}



    <table class="table">
        <thead>
            <th>ID</th>
            <th>NOMBRE</th>
            <th>CORREO</th>
            <th>TIPO</th>
            <th>ACCIÓN</th>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>
                    @if($user->type=='administrator')                    
                        <span class="label label-danger">{{$user->type}}</span>
                    @elseif($user->type=='member')
                        <span class="label label-warning">{{$user->type}}</span>
                    @endif
                </td>
                <td>
                    <a href="{{route('user.destroy',$user->id)}}" class="btn btn-danger btn-eliminar" title="Eliminar">
                    <span class="glyphicon glyphicon-trash"></span></a>
                    <a href="{{route('user.edit',$user->id)}}" class="btn btn-success" title="Editar">                   
                    <span class="glyphicon glyphicon-pencil"></span></a>
                    </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <center>
    {{$users->links()}}
    </center>
@endsection('content')

@section('javascript')
    <script>
        $('.btn-eliminar').on('click',function(event){
            event.preventDefault();
            if(confirm('Esta seguro de eliminar el Registro ?')){
                $(location).attr('href',$(this).attr('href'));
            }
            return false;
        });           
    </script>
@endsection






