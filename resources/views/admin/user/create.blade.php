@extends('admin.layouts.main')

@section('title', 'NUEVO USUARIO')

@section('content')
@include('admin.layouts.errors')
	{!! Form::open(['route'=>'user.store', 'method'=>'POST']) !!}
	<div class="form-group">
		{!! Form::label('name', 'Nombre de usuario:') !!}
		{!! Form::text('name', null, ['class'=>'form-control', 
					                  'placeholder'=>'Ingrese nombre de usuario', 'required']) !!}
	</div>
	<div class="form-group">
		{!! Form::label('email', 'Correo Electrónico:') !!}
		{!! Form::text('email',null, ['class'=>'form-control', 
					                  'placeholder'=>'ejemplo@gmail.com', 'required']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('password', 'Contraseña:') !!}
		{!! Form::password('password', ['class'=>'form-control', 'required']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('type', 'Tipo de usuario:') !!}
		{!! Form::select('type',['member'=>'Miembro','administrator'=>'Administrador'],null, 
									  ['class'=>'form-control',  'placeholder'=>'Seleccione una opción', 'required']) !!}
	</div>
	<div class="form-group">		
		{!! Form::submit('Guardar',['class'=>'btn btn-primary'])  !!}
	</div>
	

	{!! Form::close() !!}
@endsection