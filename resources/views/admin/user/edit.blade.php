@extends('admin.layouts.main')

@section('title', 'EDITAR USUARIO')

@section('content')
	{!! Form::open(['route'=>['user.update',$user], 'method'=>'PUT']) !!}
	<div class="form-group">
		{!! Form::label('name', 'Nombre de usuario:') !!}
		{!! Form::text('name', $user->name, ['class'=>'form-control', 
					                  'placeholder'=>'Ingrese nombre de usuario', 'required']) !!}
	</div>
	<div class="form-group">
		{!! Form::label('email', 'Correo Electrónico:') !!}
		{!! Form::text('email',$user->email, ['class'=>'form-control', 
					                  'placeholder'=>'ejemplo@gmail.com', 'required']) !!}
	</div>	

	<div class="form-group">
		{!! Form::label('type', 'Tipo de usuario:') !!}
		{!! Form::select('type',['member'=>'Miembro','administrator'=>'Administrador'],$user->type, 
									  ['class'=>'form-control',  'placeholder'=>'Seleccione una opción', 'required']) !!}
	</div>
	<div class="form-group">		
		{!! Form::submit('Guardar',['class'=>'btn btn-primary'])  !!}
	</div>
	

	{!! Form::close() !!}
@endsection