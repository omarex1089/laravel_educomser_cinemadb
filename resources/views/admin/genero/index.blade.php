@extends('admin.layouts.main')
@section('title','Listado Géneros de Película')
@section('content')
<a href="{{route('genero.create')}}" class="btn btn-primary">Nuevo Género</a>

{!! Form::open(['route'=>'genero.index', 'method'=>'GET',
                'CLASS'=>'navbar-form pull-right'] ) !!}
<div class="form-group">      
    <div class="input-group">
        {!! Form::text('genero',null, ['class'=>'form-control', 
                                      'placeholder'=>'Buscar por Género']) !!}        
        <div class="input-group-addon">
            <span class="glyphicon glyphicon-search"></span>
        </div>
    </div>
</div>     
{!! Form::close() !!}

 
    <table class="table">
        <thead>
            <th>ID</th>
            <th>GÉNERO</th>
            <th>ACCIÓN</th>
        </thead>
        <tbody>
            @foreach($generos as $genero)
            <tr>
                <td>{{$genero->id}}</td>
                <td>{{$genero->genero}}</td>                                
                <td>
                    <a href="{{route('genero.destroy',$genero->id)}}" class="btn btn-danger btn-eliminar" title="Eliminar">
                    <span class="glyphicon glyphicon-trash"></span></a>
                    <a href="{{route('genero.edit',$genero->id)}}" class="btn btn-success" title="Editar">                   
                    <span class="glyphicon glyphicon-pencil"></span></a>
                    </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <center>
    {{$generos->links()}}
    </center>
@endsection('content')

@section('javascript')
    <script>
        $('.btn-eliminar').on('click',function(event){
            event.preventDefault();
            if(confirm('Esta seguro de eliminar el Registro ?')){
                $(location).attr('href',$(this).attr('href'));
            }
            return false;
        });           
    </script>
@endsection






