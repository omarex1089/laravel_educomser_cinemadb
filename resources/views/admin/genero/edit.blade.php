@extends('admin.layouts.main')
@section('title', 'NUEVO GÉNERO')

@section('content')
@include('admin.layouts.errors')
	{!! Form::open(['route'=>['genero.update',$genero], 'method'=>'PUT']) !!}	
	<div class="form-group">
		{!! Form::label('genero', 'Descripción del Género:') !!}
		{!! Form::text('genero', $genero->genero, ['class'=>'form-control', 
					                  'placeholder'=>'Ingrese nombre del Género', 'required']) !!}
	</div>

	<div class="form-group">		
		{!! Form::submit('Guardar',['class'=>'btn btn-primary'])  !!}
	</div>

	{!! Form::close() !!}
@endsection