@extends('admin.layouts.main')
@section('title','Menú Principal')
@section('content')
<a href="{{ url('hola') }}">1. Hola Mundo (Mediante URL)</a><br/>
<a href="{{ route('practica1') }}">2. Hola Mundo (Mediante Nombre de Ruta)</a><br/>
{!! link_to_route('practica1',$title = '3. Hola Mundo (Mediante LaravelCollective)') !!} <br/>

<a href="{{ route('practica_blade') }}">4. Componentes Blade (Mediante Nombre de Ruta)</a><br/>
<a href="{{ route('parametros1',['nombre'=>'Alex','edad'=>45]) }}">5. Paso de Parámetros</a><br/>

@endsection('content')