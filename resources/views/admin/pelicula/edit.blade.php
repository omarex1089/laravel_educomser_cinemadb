@extends('admin.layouts.main')

@section('title', 'EDITAR PELÍCULA')

@section('content')
@include('admin.layouts.errors');
	{!! Form::open(['route'=>['pelicula.update',$pelicula], 'method'=>'PUT']) !!}
	
	<div class="form-group">
		{!! Form::label('titulo', 'Título de la Película:') !!}
		{!! Form::text('titulo', $pelicula->titulo, ['class'=>'form-control', 
					                  'placeholder'=>'Ingrese título de la Película', 'required']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('genero_id', 'Género Película:') !!}
		{!! Form::select('genero_id',$generos,$pelicula->genero_id, 
									  ['class'=>'form-control',  'placeholder'=>'Seleccione una opción', 'required']) !!}
	</div>	

	<div class="form-group">
		{!! Form::label('costo', 'Costo:') !!}
		{!! Form::number('costo',$pelicula->costo, ['class'=>'form-control','placeholder'=>'Costo','required']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('estreno', 'Fecha de Estreno:') !!}
		{!! Form::date('estreno',$pelicula->estreno, ['class'=>'form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('resumen', 'Resumen:') !!}
		{!! Form::textarea('resumen',$pelicula->resumen, ['class'=>'form-control','placeholder'=>'Ingrese Resumen de la Película','required']) !!}
	</div>	

	<div class="form-group">
		{!! Form::label('directores', 'Seleccionar directores:') !!}
		{!! Form::select('directores[]',$directores, $directores, 
									  ['class'=>'form-control','multiple', 'required']) !!}
	</div>

	<div class="form-group">		
		{!! Form::submit('Guardar',['class'=>'btn btn-primary'])  !!}
	</div>	
	
	{!! Form::close() !!}
@endsection