@extends('admin.layouts.main')

@section('title', 'NUEVA PELÍCULA')

@section('content')
@include('admin.layouts.errors')
	{!! Form::open(['route'=>'pelicula.store', 'method'=>'POST']) !!}
	
	<div class="form-group">
		{!! Form::label('titulo', 'Título de la Película:') !!}
		{!! Form::text('titulo', null, ['class'=>'form-control', 
					                  'placeholder'=>'Ingrese título de la Película', 'required']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('genero_id', 'Género Película:') !!}
		{!! Form::select('genero_id',$generos,null, 
									  ['class'=>'form-control',  'placeholder'=>'Seleccione una opción', 'required']) !!}
	</div>	

	<div class="form-group">
		{!! Form::label('costo', 'Costo:') !!}
		{!! Form::number('costo',null, ['class'=>'form-control','placeholder'=>'Costo','required']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('estreno', 'Fecha de Estreno:') !!}
		{!! Form::date('estreno',\Carbon\Carbon::now(), ['class'=>'form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('resumen', 'Resumen:') !!}
		{!! Form::textarea('resumen',null, ['class'=>'form-control','placeholder'=>'Ingrese Resumen de la Película','required']) !!}
	</div>	

	<div class="form-group">
		{!! Form::label('directores', 'Seleccionar directores:') !!}
		{!! Form::select('directores[]',$directores,null, 
									  ['class'=>'form-control','multiple', 'required']) !!}
	</div>

	<div class="form-group">		
		{!! Form::submit('Guardar',['class'=>'btn btn-primary'])  !!}
	</div>	
	
	{!! Form::close() !!}
@endsection