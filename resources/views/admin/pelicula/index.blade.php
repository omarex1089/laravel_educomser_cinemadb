@extends('admin.layouts.main')
@section('title','Lista de Peliculas')
@section('content')
<a href="{{route('pelicula.create')}}" class="btn btn-primary">Nueva Pelicula</a>

{!! Form::open(['route'=>'pelicula.index', 'method'=>'GET',
                'CLASS'=>'navbar-form pull-right'] ) !!}
<div class="form-group">      
    <div class="input-group">
        {!! Form::text('titulo',null, ['class'=>'form-control', 
                                      'placeholder'=>'Buscar por Título de película']) !!}        
        <div class="input-group-addon">
            <span class="glyphicon glyphicon-search"></span>
        </div>
    </div>
</div>     
{!! Form::close() !!}

    <table class="table">
        <thead>
            <th>ID</th>
            <th>TITULO</th>
            <th>COSTO</th>
            <th>RESUMEN</th>
            <th>ESTRENO</th>
            <th>ACCIONES</th>            
        </thead>
        <tbody>
            @foreach($peliculas as $pelicula)
            <tr>
                <td>{{$pelicula->id}}</td>
                <td>{{$pelicula->titulo}}</td>
                <td>{{$pelicula->costo}}</td>
                <td>{{$pelicula->resumen}}</td>
                <td>{{$pelicula->estreno}}</td>
                <td>
                    <a href="{{route('pelicula.destroy',$pelicula->id)}}" class="btn btn-danger btn-eliminar" title="Eliminar">
                    <span class="glyphicon glyphicon-trash"></span></a>
                    <a href="{{route('pelicula.edit',$pelicula->id)}}" class="btn btn-success" title="Editar">                   
                    <span class="glyphicon glyphicon-pencil"></span></a>
                    </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <center>
    {{$peliculas->links()}}
    </center>
@endsection('content')

@section('javascript')
    <script>
        $('.btn-eliminar').on('click',function(event){
            event.preventDefault();
            if(confirm('Esta seguro de eliminar el Registro ?')){
                $(location).attr('href',$(this).attr('href'));
            }
            return false;
        });           
    </script>
@endsection






