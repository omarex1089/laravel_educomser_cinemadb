@extends('admin.layouts.main')
@section('title','Componentes Blade')
@section('content')
    <h2>IMPRIMIR VALORES</h2>    
    <?php 
        $nombre = 'Alejandro';
        $edad = 2;
    ?>
    <p>Nombre:  {{ $nombre }}</p>
    <p>Edad:  {{ $edad }}</p>
    <p>Suma: {{ 4 + 4 }}</p>
    <p>Op. Lógica: {{ true or false}}</p>
    
    <h2>CONDICIONAL</h2>
    
    @if($edad >= 18)
        <p>Usted es Mayor de Edad</p>
    @else
        <p>Usted es Menor de Edad</p>
    @endif

    <h2>BUCLES</h2>
    <?php $nombres = array('Mateo','Marcos','Lucas','Juan'); ?>
    <ul>  
    @foreach($nombres as $nom)
        <li>{{$nom}}</li>
    @endforeach
    </ul>

    <h2>FUNCIONES:</h2>
    <p>Fecha: {{date('d/m/Y')}}</p>
@endsection('content')
        
    























