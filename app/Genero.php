<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genero extends Model
{
    // Hacer referencia al nombre de la tabla en la BD
    protected $table='generos';
    // Hacer referencia a los campos de la tabla que se utilizaran en el modelo
    protected $fillable=['genero'];

    //Adicionar Relación de un Género tiene muchas películas
    public function peliculas(){
    return $this->hasMany('App\Pelicula');
    }

    public function scopeSearch($query,$genero){
        return $query->where('genero','LIKE', '%'.$genero.'%');
    }
}
