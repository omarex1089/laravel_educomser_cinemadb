<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HolaController extends Controller
{
    public function index(){
        return 'Hola desde un Controlador 123';
    }    

    public function saludo($nombre='Anónimo'){
        return 'Hola '.$nombre.' desde el Controlador';
    }    

    public function vista(){
        return view('holavista');
    }    

    public function vistaSaludo($nombre='Ninguno'){
        return view('holasaludo')->with('nom',$nombre);
    }        

    // ENVIAR DATOS MEDIANTE FUNCIÓN WITH
    // public function vistaReporte($nombre,$edad){
    //     return view('hola.reporte')
    //         ->with('nombre',$nombre)
    //         ->with('edad',$edad);
    // }        
    
    //ENVIAR DATOS MEDIANTE UN ARRAY ASOCIATIVO
    // public function vistaReporte($nombre,$edad){
    //     $data['nombre']=$nombre;
    //     $data['edad']=$edad;
    //     return view('hola.reporte',$data);        
    // }        
    
    //ENVIAR DATOS MEDIANTE LA FUNCIÓN COMPACT
    public function vistaReporte($nombre,$edad){
        $data = compact('nombre','edad');
        return view('hola.reporte',$data);        
    }        

    public function componentesBlade(){
        return view('hola.componentes');
        
    }

}

