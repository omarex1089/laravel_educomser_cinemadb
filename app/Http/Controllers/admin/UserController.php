<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Muestra el listado de registros
        // Obtener el listado de usuarios de la base de datos
        //$users = User::all();  ---> Listado de usuarios total (no paginado)
        //$users = User::orderBy('id','ASC')->paginate(5);
        $users = User::search($request->nombre)->orderBy('id','ASC')->paginate(5);

        //dd($users); ----------------------------------> Debug
        //Enviar a la vista el listado
        return view('admin.user.index')->with('users',$users);
        

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Mostrar formulario para ingresar nuevo registro
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        // Guardar los datos de un nuevo registro
        // Recuperar datos de formulario en una Instancia
        $user = new User($request->all());
        // Cifrar password con bcrypt
        $user->password = bcrypt($user->password);
        // Guardar la instancia en la Base de datos
        $user->save();
        //return 'Usuario Registrado Exitosamente.';      
        flash('Usuario Registrado Exitosamente.')->success();
        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Muestra el detalle de un registro
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Muestra un formulario para editar un registro
        // Buscar registro a editar
        $user=User::find($id);
        // Redireccionar al formulario para editar
        //dd($user);
        return view('admin.user.edit')->with('user',$user);        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Guarda los cambios en la base de datos
        // Recuperar en un objeto el registro de la base de datos a editar
        $user=User::find($id);
        // Actualizar datos del objeto con los del formulario
        $user->name = $request->name;
        $user->email = $request->email;
        $user->type = $request->type;

        //dd($user);        
        // Guardar cambios (UPDATE)
        $user->save();  
        // Preparar mensaje para el Usuario
        flash('El usuario ha sido editado exitosamente')->success();  
        // Redireccionar al listado
        return redirect()->route('user.index');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Elimina un registro de la base de datos
        // Buscar el usuario a eliminar
           $user = User::find($id); 
        // Eliminar el usuario
           $user->delete();  
        // Preparar el mensaje
           flash('El usuario se ha eliminado correctamente')->success();  
        // Redireccionar al listado de usuarios
           return redirect()->route('user.index');
    }
}
