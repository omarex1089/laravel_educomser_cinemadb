<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Pelicula;
use App\Genero;
use App\Director;
use Auth;
use App\Http\Requests\PeliculaRequest;

class PeliculaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
          //$users = User::search($request->nombre)->orderBy('id','ASC')->paginate(5);
        $peliculas = Pelicula::search($request->titulo)->orderBy('id','ASC')->paginate(5);
        return view('admin.pelicula.index')->with('peliculas',$peliculas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $generos = Genero::orderBy('genero','ASC')->pluck('genero','id');
        $directores = Director::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('admin.pelicula.create')->with('generos',$generos)
                                            ->with('directores',$directores);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PeliculaRequest $request)
    {
        $pelicula = new Pelicula($request->all());        
        $pelicula->user_id = 1;                
        $pelicula->save();        
        $pelicula->directores()->sync($request->directores);
        flash('Película Registrada Exitosamente.')->success();        
        return redirect()->route('pelicula.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $generos = Genero::orderBy('genero','ASC')->pluck('genero','id');
        $directores = Director::orderBy('nombre','ASC')->pluck('nombre','id');
        $pelicula=Pelicula::find($id);
        return view('admin.pelicula.edit')->with('pelicula',$pelicula)
                                          ->with('generos',$generos)
                                          ->with('directores',$directores);                              
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pelicula=Pelicula::find($id);        
        $pelicula->titulo = $request->titulo;
        $pelicula->costo = $request->costo;
        $pelicula->resumen = $request->resumen;
        $pelicula->estreno = $request->estreno;
        $pelicula->genero_id = $request->genero_id;
        //dd($user);        
        // Guardar cambios (UPDATE)
        $pelicula->save();  
        // Preparar mensaje para el Usuario
        flash('La película ha sido editado exitosamente')->success();  
        // Redireccionar al listado
        return redirect()->route('pelicula.index');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {         
        $pelicula = Pelicula::find($id); 
        $pelicula->delete();          
        flash('Película eliminada correctamente')->success();          
        return redirect()->route('pelicula.index');               
    }
}
