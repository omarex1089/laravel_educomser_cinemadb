<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // Activar validación (cambio de false a true)
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Adicionar reglas de Validación
        return [
            'name'=>'min:3|max:100|required',
            //Users es el nombre de la tabla
            'email'=>'min:4|max:100|required|unique:users|email',
            'password'=>'min:6|max:100|required'
        ];
    }
}
