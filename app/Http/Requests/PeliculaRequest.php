<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PeliculaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Adicionar reglas de Validación
        return [
            'titulo'=>'min:3|max:100|required',            
            'costo'=>'integer',
            'resumen'=>'min:10|max:1000|required',
            'estreno'=>'required|date'
        ];
    }
}
