<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Director extends Model
{
      // Hacer referencia al nombre de la tabla en la BD
      protected $table='directores';
      // Hacer referencia a los campos de la tabla que se utilizaran en el modelo
      protected $fillable=['nombre'];
    
     //Adicionar Relación: Undirector puede tener muchas películas
      public function peliculas(){
        return $this->belongsToMany('App\Pelicula','pelicula_director');
    }

    public function scopeSearch($query,$nombre){
      return $query->where('nombre','LIKE', '%'.$nombre.'%');
    }
}
