<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Imagen extends Model
{
    // Hacer referencia al nombre de la tabla en la BD
    protected $table='imagenes';
    // Hacer referencia a los campos de la tabla que se utilizaran en el modelo
    protected $fillable=['nombre','pelicula_id'];
    
     //Adicionar Relación: Una imagen pertenece a una sola película
     public function pelicula(){
        return $this->belongsTo('App\Pelicula');
    }    
}

