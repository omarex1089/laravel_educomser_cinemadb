<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelicula extends Model
{
    // Hacer referencia al nombre de la tabla en la BD
    protected $table='peliculas';
    // Hacer referencia a los campos de la tabla que se utilizaran en el modelo
    protected $fillable=['titulo','costo','resumen','estreno','genero_id'];

    //Adicionar Relación: Una película pertenece a un usuario
    public function user(){
        return $this->belongsTo('App\User');
    }
    //Adicionar Relación: Una película pertenece a un género
    public function genero(){
        return $this->belongsTo('App\Genero');
    }
    //Adicionar Relación: Una película puede tener muchas imágeners
    public function imagenes(){
        return $this->hasMany('App\Imagen');
    }

    //Adicionar Relación: Una película puede tener muchos directores
    public function directores(){
        return $this->belongsToMany('App\Director','pelicula_director')->withTimestamps();
    }

    public function scopeSearch($query,$titulo){
        return $query->where('titulo','LIKE', '%'.$titulo.'%');
    }

}

