<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Primera ruta hola mundo ---------------------------- (1)
Route::get('/hola', function () {
    return 'Hola mundo desde web.php';
})->name('practica1');

// Paso de parámetros  ---------------------------- (2)
Route::get('/detalle/{nombre}/{edad}', function ($nombre, $edad) {
    return 'Nombre: '.$nombre.'<br/> Edad: '.$edad;
})->name('parametros1');

// Paso de parámetros Valores por defecto ---------------------------- (3)
Route::get('/reporte/{nombre?}', function ($nombre='Ninguno') {
    return 'Nombre: '.$nombre;
});

// Grupo de Rutas (Agrupar Rutas)
/* Route::get('/saludo/dia', function () {
    return 'Buenos dias';
});
Route::get('/saludo/tarde', function () {
    return 'Buenas tardes';
});
Route::get('/saludo/noche', function () {
    return 'Buenas noches';
});
 */

 // Rutas con Prefijo                       ---------------------------- (4)
Route::group(['prefix'=>'saludo'],function(){
    Route::get('dia', function () {
        return 'Buenos dias';
    });
    Route::get('tarde', function () {
        return 'Buenas tardes';
    });
    Route::get('noche', function () {
        return 'Buenas noches';
    });
});

// Invocando método de un controlador    ---------------------------- (5)
Route::get('/hola/mundo','HolaController@index');

// Invocando método de un controlador con parámetros ---------------------------- (6)
Route::get('/hola/saludo/{nombre?}','HolaController@saludo');

// Renderizar una vista desde el controlador ---------------------------- (7)
Route::get('/hola/vista','HolaController@vista');

// Enviar dato de controlador a una vista ---------------------------- (8)
Route::get('/hola/vista/saludo/{nombre?}','HolaController@vistaSaludo');

// Enviar datos a una Vista ---------------------------- (9)
Route::get('/hola/vista/saludo/{nombre}/{edad}','HolaController@vistaReporte');

// Componentes de Blade  ---------------------------- (10)
Route::get('/hola/blade','HolaController@componentesBlade')->name('practica_blade');


// RUTAS ADMIN
Route::group(['prefix'=>'admin'],function(){
    Route::get('home','admin\HomeController@index')->name('inicio'); 
     // RUTAS RESTFUL - MODULO DE USUARIO
    Route::resource('user','admin\UserController');   
    Route::get('user/{id}/destroy','admin\UserController@destroy')->name('user.destroy');
    // RUTAS RESTFUL - MODULO DE PELICULAS
    Route::resource('pelicula','admin\PeliculaController');   
    Route::get('pelicula/{id}/destroy','admin\PeliculaController@destroy')->name('pelicula.destroy');
    // RUTAS RESTFUL - MODULO DE DIRECTORES
    Route::resource('director','admin\DirectorController');   
    Route::get('director/{id}/destroy','admin\DirectorController@destroy')->name('director.destroy');
    // RUTAS RESTFUL - MODULO DE GÉNEROS
    Route::resource('genero','admin\GeneroController');   
    Route::get('genero/{id}/destroy','admin\GeneroController@destroy')->name('genero.destroy');    
});











































