-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-06-2020 a las 01:53:37
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cinemadb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directores`
--

CREATE TABLE `directores` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `directores`
--

INSERT INTO `directores` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(7, 'Martin Scorsese', '2020-06-22 20:51:45', '2020-06-22 20:53:21'),
(8, 'Steven Spielberg', '2020-06-22 20:51:59', '2020-06-22 20:51:59'),
(9, 'Roman Polanski', '2020-06-22 20:52:13', '2020-06-22 20:52:13'),
(10, 'Francis Ford Coppola', '2020-06-22 20:52:24', '2020-06-22 20:52:24'),
(11, 'Alfred Hitchcock', '2020-06-22 20:52:33', '2020-06-22 20:52:33'),
(12, 'Fritz Lang', '2020-06-22 20:52:50', '2020-06-22 20:52:50'),
(13, 'Woody Allen', '2020-06-22 20:53:07', '2020-06-22 20:53:07'),
(15, 'Stanley Kubrick', '2020-06-22 20:54:18', '2020-06-22 20:54:18'),
(16, 'Charles Chaplin', '2020-06-22 20:54:34', '2020-06-22 20:54:34'),
(17, 'Quentin Tarantino', '2020-06-22 20:54:54', '2020-06-22 20:54:54'),
(18, 'Tim Burton', '2020-06-22 20:55:13', '2020-06-22 20:55:13'),
(19, 'Christopher Nolan', '2020-06-22 20:55:50', '2020-06-22 20:55:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `generos`
--

CREATE TABLE `generos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `genero` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `generos`
--

INSERT INTO `generos` (`id`, `genero`, `created_at`, `updated_at`) VALUES
(5, 'Comedia', '2020-06-22 20:30:09', '2020-06-22 20:30:09'),
(6, 'Aventura', '2020-06-22 20:30:59', '2020-06-22 20:30:59'),
(7, 'Ciencia Ficción', '2020-06-22 20:39:33', '2020-06-22 20:47:02'),
(9, 'Terror', '2020-06-22 20:48:00', '2020-06-22 20:48:00'),
(10, 'Suspenso', '2020-06-22 20:48:06', '2020-06-22 20:50:31'),
(11, 'Musical', '2020-06-22 20:48:12', '2020-06-22 20:48:12'),
(12, 'Acción', '2020-06-22 20:48:18', '2020-06-22 20:48:18'),
(13, 'Western', '2020-06-22 20:48:31', '2020-06-22 20:48:31'),
(14, 'Drama', '2020-06-22 20:48:47', '2020-06-22 20:48:47'),
(15, 'Melodrama', '2020-06-22 20:48:56', '2020-06-22 20:48:56'),
(16, 'Romance', '2020-06-22 20:49:01', '2020-06-22 20:49:01'),
(17, 'Histórico', '2020-06-22 20:49:15', '2020-06-22 20:49:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes`
--

CREATE TABLE `imagenes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pelicula_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(6, '2014_10_12_000000_create_users_table', 1),
(7, '2014_10_12_100000_create_password_resets_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2020_06_03_224903_create_generos_table', 1),
(10, '2020_06_03_231519_create_peliculas_table', 1),
(11, '2020_06_04_184107_create_directores_table', 2),
(12, '2020_06_04_190449_create_imagenes_table', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peliculas`
--

CREATE TABLE `peliculas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `costo` double(8,2) NOT NULL,
  `resumen` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `estreno` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `genero_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `peliculas`
--

INSERT INTO `peliculas` (`id`, `titulo`, `costo`, `resumen`, `estreno`, `created_at`, `updated_at`, `genero_id`, `user_id`) VALUES
(17, 'Batman: El caballero de la noche', 150.00, 'El crimen organizado se ha convertido en el problema más irritante en Gotham City, por lo que Batman y el teniente James Gordon (Gary Oldman) contemplan la posibilidad de incluir al nuevo fiscal de distrito, Harvey Dent (Aaron Eckhart), en su plan para combatir a la mafia, poniendo a Dent como el héroe público que Batman no puede ser, al tiempo que Bruce descubre que Dent es el nuevo novio de Rachel Dawes (Maggie Gyllenhaal).', '2008-07-14', '2020-06-22 20:57:50', '2020-06-22 21:01:35', 12, 1),
(18, 'Psicosis', 200.00, 'Es una película estadounidense de terror y suspenso dirigida por Alfred Hitchcock, protagonizada por Anthony Perkins, Vera Miles, John Gavin, Martin Balsam y Janet Leigh y estrenada por primera vez en cines en 1960. El guion, de Joseph Stefano, se basa en la novela homónima de 1959 escrita por Robert Bloch, que a su vez fue inspirada por los crímenes de Ed Gein, un asesino en serie de Wisconsin.', '1960-01-22', '2020-06-22 21:06:30', '2020-06-22 21:06:30', 10, 1),
(19, '2001: Odisea del espacio', 300.00, 'La secuencia inicial del filme se inicia con la imagen de la Tierra ascendiendo sobre la Luna, mientras que el Sol asciende a su vez sobre la Tierra, todos en alineación. En este momento comienza a escucharse la composición musical Así habló Zaratustra, de Richard Strauss, la misma que acompaña, en su mayoría, la primera parte de la película, titulada El amanecer del hombre.', '1968-04-02', '2020-06-22 21:08:13', '2020-06-22 21:08:13', 7, 1),
(20, 'Taxi Driver', 180.00, 'Travis Bickle es un exmarine solitario y deprimido que, luego de retornar de la Guerra de Vietnam, vive en la ciudad de Nueva York de mediados de los años 70. Al padecer de insomnio crónico, se pone a trabajar como taxista, conduce pasajeros cada noche por los suburbios. También pasa tiempo en cines porno de mala muerte y escribe un diario.', '1976-02-08', '2020-06-22 21:09:35', '2020-06-22 21:09:52', 14, 1),
(21, 'El Padrino', 210.00, 'El padrino (título original en inglés: The Godfather1​) es una película estadounidense de 1972 dirigida por Francis Ford Coppola. La película fue producida por Albert S. Ruddy, de la compañía Paramount Pictures. Está basada en la novela homónima (que a su vez está basada en la familia real de los Mortillaro de Sicilia, Italia)', '1972-03-15', '2020-06-22 21:11:14', '2020-06-22 21:11:14', 14, 1),
(22, 'La quimera del oro', 150.00, 'Charlot está en el Klondike, atraído por la fiebre del oro. Se desata una tormenta, se ve obligado a buscar refugio y encuentra una casa aislada en las montañas, habitada por un homicida fugado, Black Larsen, que trata de echar al vagabundo, pero un vendaval se lo impide y además trae otro huésped: el gigante Mac Kay. Tras una pelea en que queda inutilizado el rifle del fugitivo, los dos huéspedes logran quedarse.', '1925-06-26', '2020-06-22 21:12:40', '2020-06-22 21:12:40', 5, 1),
(23, 'Kill Bill', 199.00, 'En Kill Bill: Volumen 1, una mujer embarazada, identificada como la Novia, es masacrada y tiroteada en la cabeza, en el ensayo de su boda en una pequeña iglesia al sur de Texas, por sus antiguos camaradas del Escuadrón Asesino Víbora Letal, un grupo de asesinos profesionales que trabajan para el mejor postor, venganza, guerra de bandas de traficantes de drogas y asesinatos políticos. Por la venganza de su exnovio Bill, que fue abandonado por la Novia y trataba de alejarse de la banda, para iniciar una nueva vida alejada de la violencia.', '2003-09-01', '2020-06-22 21:14:05', '2020-06-22 21:14:05', 12, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pelicula_director`
--

CREATE TABLE `pelicula_director` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pelicula_id` bigint(20) UNSIGNED NOT NULL,
  `director_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `pelicula_director`
--

INSERT INTO `pelicula_director` (`id`, `pelicula_id`, `director_id`, `created_at`, `updated_at`) VALUES
(1, 17, 19, '2020-06-22 20:57:50', '2020-06-22 20:57:50'),
(2, 18, 11, '2020-06-22 21:06:30', '2020-06-22 21:06:30'),
(3, 19, 15, '2020-06-22 21:08:13', '2020-06-22 21:08:13'),
(4, 20, 7, '2020-06-22 21:09:35', '2020-06-22 21:09:35'),
(5, 21, 10, '2020-06-22 21:11:14', '2020-06-22 21:11:14'),
(6, 22, 16, '2020-06-22 21:12:40', '2020-06-22 21:12:40'),
(7, 23, 17, '2020-06-22 21:14:05', '2020-06-22 21:14:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('member','administrator') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'member',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Alberto', 'alberto@gmail.com', NULL, '$2y$10$KK3lXvR7p7tQZnR55VstZuXnPR20OZMSAeqXJPM5eN1O9WhBiDt.C', 'administrator', NULL, '2020-06-22 14:44:04', '2020-06-22 14:44:04'),
(30, 'Asistente', 'asistente@gmail.com', NULL, '$2y$10$BaJQFQeahMLlBGdIQxekRetdD7CZnDHeKGPUlDviTZM245IP8uaW.', 'member', NULL, '2020-06-22 22:45:47', '2020-06-22 22:45:47');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `directores`
--
ALTER TABLE `directores`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `generos`
--
ALTER TABLE `generos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `imagenes`
--
ALTER TABLE `imagenes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `imagenes_pelicula_id_foreign` (`pelicula_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `peliculas`
--
ALTER TABLE `peliculas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `peliculas_genero_id_foreign` (`genero_id`),
  ADD KEY `peliculas_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `pelicula_director`
--
ALTER TABLE `pelicula_director`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pelicula_director_pelicula_id_foreign` (`pelicula_id`),
  ADD KEY `pelicula_director_id_foreign` (`director_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `directores`
--
ALTER TABLE `directores`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `generos`
--
ALTER TABLE `generos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `imagenes`
--
ALTER TABLE `imagenes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `peliculas`
--
ALTER TABLE `peliculas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `pelicula_director`
--
ALTER TABLE `pelicula_director`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `imagenes`
--
ALTER TABLE `imagenes`
  ADD CONSTRAINT `imagenes_pelicula_id_foreign` FOREIGN KEY (`pelicula_id`) REFERENCES `peliculas` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `peliculas`
--
ALTER TABLE `peliculas`
  ADD CONSTRAINT `peliculas_genero_id_foreign` FOREIGN KEY (`genero_id`) REFERENCES `generos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `peliculas_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `pelicula_director`
--
ALTER TABLE `pelicula_director`
  ADD CONSTRAINT `pelicula_director_id_foreign` FOREIGN KEY (`director_id`) REFERENCES `directores` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pelicula_director_pelicula_id_foreign` FOREIGN KEY (`pelicula_id`) REFERENCES `peliculas` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
