<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDirectoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('directores', function (Blueprint $table) {
            $table->bigIncrements('id');          
            $table->string('nombre');            
            $table->timestamps();
        });

        Schema::create('pelicula_director', function (Blueprint $table) {
            $table->bigIncrements('id');          
            //Campos para los FK
            $table->bigInteger('pelicula_id')->unsigned(); 
            $table->bigInteger('director_id')->unsigned(); 
            //Relacionamos las tablas
            $table->foreign('pelicula_id')->references('id')->on('peliculas')->onDelete('cascade');
            $table->foreign('id')->references('id')->on('directores')->onDelete('cascade');;
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelicula_director');
        Schema::dropIfExists('directores');        
    }
}
