<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeliculasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peliculas', function (Blueprint $table) {
            // Columnas básicas de la tabla
            $table->bigIncrements('id');
            $table->string('titulo');
            $table->float('costo');
            $table->text('resumen');
            $table->date('estreno');            
            $table->timestamps();
            // Agregar relación 1aN con la tabla géneros
            $table->bigInteger('genero_id')->unsigned();                        
            $table->foreign('genero_id')->references('id')->on('generos')->onDelete('cascade');
            // Agregar relación 1aN con la tabla users
            $table->bigInteger('user_id')->unsigned();                        
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peliculas');
    }
}
